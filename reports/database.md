# La base de Donne

la base de donne suit le modele MCD suivant:
## Conception de base de donne
Code SQL de creation:
```
CREATE TABLE professors (
    id INT,
    first_name VARCHAR(15),
    last_name VARCHAR(15),
    email VARCHAR(30),
    passphrase VARCHAR(40)
);

create table classes (
    id int,
    class_name varchar(7),
    full_name varchar(100)
);

create table courses (
    id int,
    course_name varchar(100),
    course_semester tinyint
);

create table students (
    id int,
    first_name varchar(15),
    last_name varchar(15),
    email varchar(15),
    passphrase varchar(40),

    class_id int
);
create table notifications (
    id int,
    notification_content varchar(200),
    notification_status varchar(10),
    notification_date date,

    student_id int,
    class_id int
);

create table grades (
    student_id int,
    course_id int,
    grade_status varchar(20),
    grade tinyint
);

create table teaching (
    id_professor int,
    id_class int,
    id_course int
);
```
On ajoute les contraintes pour chaque table
```
code goes here
```

## Utilisation de base de donnee
